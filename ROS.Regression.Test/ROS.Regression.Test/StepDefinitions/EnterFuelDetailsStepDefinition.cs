﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ROS.Test.Shared.Helpers;
using TechTalk.SpecFlow;

namespace ROS.Regression.Test.StepDefinitions
{
    /// <summary>
    /// This class contains the step definition for Enter fuel information scenerios
    /// </summary>
    [Binding]
    public  class EnterFuelDetailsStepDefinition
    {
        #region Properties

        private readonly GeneralHelpers generalHelpers = new GeneralHelpers();
        private readonly ScenarioContext _scenarioContext;

        #endregion Properties

        #region Constructor

        public EnterFuelDetailsStepDefinition(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        #endregion Constructor

        #region Stepdefinitions

        [Given(@"I select the ""(.*)"" as (.*)")]
        public void GivenISelectTheAs(string inputfield, string input)
        {
            generalHelpers.SelectElementFromDropdown(inputfield, input);        
        }

        [Given(@"I enter the ""(.*)"" as (.*)")]
        public void GivenIEnterTheAs(string inputfield, string input)
        {
            generalHelpers.EnterInput(inputfield, input);
        }


        [When(@"I click Submit")]
        public void WhenIClickSubmit()
        {
            generalHelpers.ClickElement("Continue button");
        }

        [Then(@"I should be see the ""(.*)"" page")]
        public void ThenIShouldBeSeeThePage(string pagetitle)
        {
            Assert.AreEqual(
               pagetitle
               , this.generalHelpers.GetTextFromElement("Sustainability page heading")
               , "The page title '{0}' is incorrect"
               , pagetitle);
        }

        #endregion Stepdefinitions
    }
}