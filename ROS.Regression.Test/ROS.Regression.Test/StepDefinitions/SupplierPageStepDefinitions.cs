﻿using NUnit.Framework;
using ROS.Test.Shared.Helpers;
using TechTalk.SpecFlow;

namespace ROS.Regression.Test.StepDefinitions
{
    /// <summary>
    /// This class contains the step definitions for supplier page tests
    /// </summary>
    [Binding]
    public class SupplierPageStepDefinitions
    {     
        #region Properties 

        private readonly GeneralHelpers generalHelpers = new GeneralHelpers();
        private readonly ScenarioContext _scenarioContext;

        #endregion Properties

        #region Constructor

        public SupplierPageStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        #endregion Constructor     

        #region Stepdefinitions    

        [Then(@"I should see the ""(.*)""")]
        public void ThenIShouldSeeThe(string pagetitle)
        {      
            Assert.AreEqual(
                pagetitle
                , this.generalHelpers.GetTextFromElement("Supplier page heading")
                , "The page title '{0}' is incorrect"
                , pagetitle);
        }

        #endregion Stepdefinitions
    }
}
