﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ROS.Test.Shared.Helpers;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ROS.Test.Shared.Utilities
{
    /// <summary>
    /// This class contains the methods commonly used across all project
    /// </summary>
    public static class Commonlib
    {
        #region Methods

        /// <summary>
        /// This method will be used to wait for page load with the value provided from the constant utils     
        /// </summary>
        public static void WaitForPageLoad()
        {
            try
            {
                // this.Log.Info(h => h("Waiting until Element is displayed"));

                Driver.Instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(ConstantUtils.WAITFORPAGELOAD);
                //IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(ConstantUtils.WAITFORPAGELOAD));
            }      

            catch (Exception e)
            {
                // this.Log.Info(h => h("Element did not display with message : {0} ", e.Message));
                Assert.Fail("Page not Loadedmessage : {0} ", e.Message);
            }
        }

        /// <summary>
        /// This will search for the element until a timeout is reached
        /// </summary>
        /// <param name="elementCssSelector"></param>
        /// <param name="timeout"></param>
        public static void WaitUntilElementExists(string elementLocator)
        {
            new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(ConstantUtils.WAITFORELEMENT))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.CssSelector(elementLocator)));
        }

        /// <summary>
        /// This will wait for the element to be clickable until a timeout is reached
        /// </summary>
        /// <param name="elementCssSelector"></param>
        /// <param name="timeout"></param>
        public static void WaitUntilElementIsClickable(string elementLocator)
        {
            new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(ConstantUtils.WAITFORELEMENT))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.CssSelector(elementLocator)));
        }

        #endregion Methods
    }
}
