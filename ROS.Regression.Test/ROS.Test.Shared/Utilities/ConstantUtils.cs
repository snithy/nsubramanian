﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ROS.Test.Shared.Utilities
{
    public class ConstantUtils
    {
        /// <summary>
        /// Base url of the site to be passed from jenkins job
        /// </summary>
        public static string BaseUrl = Environment.GetEnvironmentVariable("BASEURL");

        //public const string BaseUrl = "https://roslite-Live.herokuapp.com";
        //public const string BaseUrl = "https://gos-integration.triad.co.uk";
        public static string BrowserType = Environment.GetEnvironmentVariable("BROWSERTYPE");
        //public static string BrowserType = "Chrome";

        /// this will set the wait time that can used across all the tests
        public const int WAITFORPAGELOAD = 60;
        public const int WAITFORELEMENT = 40;
        public const int IMPLICITWAIT = 30;
    }
}
