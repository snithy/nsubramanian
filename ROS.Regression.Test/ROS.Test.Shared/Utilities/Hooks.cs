﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenQA.Selenium;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Support.Extensions;

namespace ROS.Test.Shared.Utilities
{
    [Binding]
    public class Hooks : Driver
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks
      
        public static string scenarioName;
        public static string featureName;

        private readonly ScenarioContext _scenarioContext;

        public Hooks(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            Driver.Initialize();
        }

        //[BeforeFeature]
        //public void BeforeFeature()
        //{
        //    Driver.Initialize();
        //}

        //[AfterFeature]
        //public void AfterFeature()
        //{
        //    Driver.Close();
        //}

        [AfterScenario]
        public void AfterScenario()
        {
            try
            {
                if (_scenarioContext.TestError != null)
                {
          Screenshot ss = Instance.TakeScreenshot();
                    Console.WriteLine("After screenshot" + ScenarioExecutionStatus.TestError.ToString());
                    string title = _scenarioContext.ScenarioInfo.Title;
                    string runname = title + DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss");
                    //string screenshotDirectory = Environment.CurrentDirectory;
                    //string screenshotDirectory = Environment.CurrentDirectory.Replace("bin/Debug/netcoreapp3.1", "TestData");
                     string screenshotDirectory = Environment.CurrentDirectory.Replace("bin/Debug/netcoreapp3.1", "TestResults");
                    Directory.CreateDirectory(screenshotDirectory);

                    var screenshotfilename = screenshotDirectory + "/" + runname + ".png";

                    ss.SaveAsFile(screenshotfilename, ScreenshotImageFormat.Png);
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("catch");
                throw e;
            }
            finally
            {
                if (Instance != null)
                {
                    Instance.Quit();
                    Instance = null;
                }
            }
        }
    }
}
