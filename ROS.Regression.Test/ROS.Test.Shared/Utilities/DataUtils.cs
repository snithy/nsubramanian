﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Resources;
using System.Text;

namespace ROS.Test.Shared.Utilities
{
    public static class DataUtils
    {
        public static string GetResourceValue(string name)
        {
            //ResourceManager rm = new ResourceManager("ROS.Regression.Test.TestData.TestResource", Assembly.LoadFrom(@"..\netcoreapp3.1\ROS.Regression.Test.dll"))
          
            ResourceManager rm = new ResourceManager("ROS.Test.Shared.TestData.TestResource", Assembly.GetExecutingAssembly());
          
            return rm.GetString(name);
        }
    }
}
