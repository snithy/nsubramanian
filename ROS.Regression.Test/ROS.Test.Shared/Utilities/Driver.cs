﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace ROS.Test.Shared.Utilities
{
    public class Driver
    {
        #region Properties

        public static IWebDriver Instance { get; set; }

        public static string baseDirPath = System.AppDomain.CurrentDomain.BaseDirectory;
        public static string filePath = baseDirPath.Replace("netcoreapp3.1", "DownloadedCsv");

        public static string BrowserType
        {
            get { return ConstantUtils.BrowserType; }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Initialise "Chrome driver".
        /// Set implicit wait to 10 seconds.
        /// Maximise the browser window
        /// </summary>I have cloned the
        public static void Initialize()
        {
            {
                var browserType = BrowserType.ToLower();
                switch (browserType)
                {
                    case "firefox":
                        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                        FirefoxProfile firefoxprofile = new FirefoxProfile();
                        firefoxprofile.SetPreference("browser.download.dir", filePath);
                        firefoxprofile.SetPreference("browser.download.folderList", 2);
                        firefoxprofile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "txt csv text/csv");

                        FirefoxOptions firefoxoptions = new FirefoxOptions() { Profile = firefoxprofile };
                        firefoxoptions.AddArgument("--headless");
                        firefoxoptions.AddArgument("--disable-dev-shm-usage");
                        firefoxoptions.AddArgument("--no-sandbox");

                        Instance = new FirefoxDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), firefoxoptions, TimeSpan.FromMinutes(3));
                        break;
                    case "internetexplorer":
                    case "ie":
                        InternetExplorerOptions ieoptions = new InternetExplorerOptions
                        {
                            EnableNativeEvents = false
                            ,
                            IntroduceInstabilityByIgnoringProtectedModeSettings = true
                            ,
                            EnsureCleanSession = true
                            ,
                            EnablePersistentHover = false
                        };
                        ieoptions.AddAdditionalCapability("disable-popup-blocking", true);
                        ieoptions.PageLoadStrategy = PageLoadStrategy.Eager;
                        ieoptions.IgnoreZoomLevel = true;
                        Instance = new InternetExplorerDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), ieoptions);
                        break;
                    default:
                        ChromeOptions chromeoptions = new ChromeOptions();
                        chromeoptions.AddArgument("headless");
                        chromeoptions.AddArgument("--disable-dev-shm-usage");
                        chromeoptions.AddArgument("--no-sandbox");
                        chromeoptions.AddUserProfilePreference("download.default_directory", filePath);
                        //chromeoptions.UseSpecCompliantProtocol = false;
                        //chromeoptions.SetLoggingPreference(LogType.Browser, LogLevel.All);
                      //var remoteUrl = "http://localhost:4444/wd/hub";
                       //Instance = new RemoteWebDriver(new Uri(remoteUrl), chromeoptions);
                       //Instance.Navigate().GoToUrl("https://gos-integration.triad.co.uk");
                        Instance = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeoptions, TimeSpan.FromMinutes(3));
                        break;
                }

                Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                Instance.Manage().Window.Maximize();
                Instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
            }


            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(ConstantUtils.WAITFORELEMENT);
            Instance.Manage().Window.Maximize();
            Instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(ConstantUtils.WAITFORPAGELOAD);
        }   
  
        /// <summary>
        
        /// Close the opened browser window
        /// </summary>
        public static void Close()
        {
            Instance.Dispose();
        }

        #endregion Methods
    }
}

