﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using ROS.Test.Shared.Utilities;

namespace ROS.Test.Shared.Helpers
{
    /// <summary>
    /// This class contains the general helper methods commonly used in all projects
    /// </summary>
    
    public class GeneralHelpers
    {
        #region Methods

        /// <summary>
        /// Gets css selectors (locators) from Locators.json
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Css selector of a given element from locators.json file</returns>
        public static string GetLocator(string key)
        {
            // Open the json file
            var stream = File.OpenText("PageObjects/Locators.json");

            // Read the file
            string json = stream.ReadToEnd();
            JObject obj = JObject.Parse(json);

            return (string)obj[key];
        }

        /// <summary>
        /// Identify the element using the elementCssSelector and Enter the input in the field using sendkeys
        /// </summary>
        /// <param name="elementCssSelector"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public void EnterInput(string elementCssSelector, string input)
        {
            string elementLocator = GeneralHelpers.GetLocator(elementCssSelector);
            Commonlib.WaitUntilElementExists(elementLocator);
            Driver.Instance.FindElement(By.CssSelector(elementLocator)).Clear();
            Driver.Instance.FindElement(By.CssSelector(elementLocator)).SendKeys(input);
        }

        /// <summary>
        /// Identify the element using the elementCssSelector and click the element
        /// </summary>
        /// <param name="elementCssSelector"></param>        /// 
        /// <returns></returns>
        public void ClickElement(string elementCssSelector)
        {
            string elementLocator = GeneralHelpers.GetLocator(elementCssSelector);
            Commonlib.WaitUntilElementExists(elementLocator);
            Driver.Instance.FindElement(By.CssSelector(elementLocator)).Click();
            Commonlib.WaitForPageLoad();
        }

        /// <summary>
        /// Identify the element using the elementCssSelector and return the text value from the element
        /// </summary>
        /// <param name="elementCssSelector"></param>
        /// <returns>The text of the element</returns>
        public string GetTextFromElement(string elementCssSelector)
        {
            string elementLocator = GeneralHelpers.GetLocator(elementCssSelector);
            Commonlib.WaitUntilElementExists(elementLocator);
            return Driver.Instance.FindElement(By.CssSelector(elementLocator)).Text;          
        }

        /// <summary>
        /// Click on the dropdown list and select an item 
        /// </summary>
        /// <param name="elementCssSelector"></param>
        /// <param name="item"></param>
        /// <returns></returns>        
        public void SelectElementFromDropdown(string elementCssSelector, string item)
        {
            string elementLocator = GeneralHelpers.GetLocator(elementCssSelector);
            string elementItemLocator = GeneralHelpers.GetLocator(elementCssSelector + " " +  "options");
            Commonlib.WaitUntilElementExists(elementLocator);
            var dropdown = Driver.Instance.FindElement(By.CssSelector(elementLocator));
            dropdown.SendKeys(item);
            //var items = dropdown.FindElements(By.XPath("(//*[contains(@id,'ac-apply-fuelType__option')]"));
            var items = dropdown.FindElements(By.XPath(elementItemLocator));
            string a = items[0].Text;
            //Assert.AreEqual(a, item);
            dropdown.FindElements(By.XPath(elementItemLocator)).Single(t => t.Text.Trim().Contains(item)).Click();
        }

        /// <summary>
        /// Steps to login using the supplier account
        /// </summary>
        /// <param></param>  
        /// <returns></returns>  
        public void LoginAsSupplier()
        {
            this.NavigateToLoginPage();
           this.EnterInput("Email address field", DataUtils.GetResourceValue("ValidUsername"));
            this.EnterInput("Password field", DataUtils.GetResourceValue("ValidPassword"));           
            this.ClickElement("Sign in button");
        }

        /// <summary>
        /// Steps to navigate to the Login page
        /// </summary>
        /// <param></param>  
        /// <returns></returns> 
        public void NavigateToLoginPage()
        {
            Driver.Instance.Navigate().GoToUrl(ConstantUtils.BaseUrl + "/app/account/sign-in");
            Commonlib.WaitForPageLoad();        
        }

        /// <summary>
        /// Steps to navigate to the Fuel information page
        /// </summary>
        /// <param></param>  
        /// <returns></returns> 
        public void NavigateToFuelInformationPage()
        {         
            Driver.Instance.FindElement(By.LinkText("Submit a single application")).Click();
            this.ClickElement("Continue button");
        }

        #endregion Methods
    }
}