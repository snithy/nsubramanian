﻿using ROS.Test.Shared.Helpers;
using ROS.Test.Shared.Utilities;
using TechTalk.SpecFlow;

namespace ROS.Test.Shared.StepDefinitions
{
    /// <summary>
    /// This class contains commonly used step definitions for login steps in all specflow UI test projects
    /// </summary>
    [Binding]
    public class LoginStepDefinition
    {    
        #region Properties  
        
        private readonly GeneralHelpers generalHelpers = new GeneralHelpers();

        private readonly ScenarioContext _scenarioContext;

        #endregion Properties

        #region Constructor

        public LoginStepDefinition(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        #endregion Constructor

        #region Stepdefinitions

        [Given(@"I navigate to the ROS login ""(.*)""")]
        public void GivenINavigateToTheROSLogin(string path)
        {
            Driver.Instance.Navigate().GoToUrl(ConstantUtils.BaseUrl + path);          
            Commonlib.WaitForPageLoad();
        }

        [When(@"I enter my username (.*)")]
        public void WhenIEnterMyUsername(string username)
        {
            this.generalHelpers.EnterInput("Email address field", DataUtils.GetResourceValue(username));
        }

        [When(@"I enter my password (.*)")]
        public void WhenIEnterMyPassword(string password)
        {
            generalHelpers.EnterInput("Password field", DataUtils.GetResourceValue(password));
        }

        [When(@"I click Login buton")]
        public void WhenIClickLoginButon()
        {
            generalHelpers.ClickElement("Sign in button");
        }

        [When(@"I click Showpassword buton")]
        public void WhenIClickShowpasswordButon()
        {
            generalHelpers.ClickElement("Show password button");
        }

        [Given(@"I have logged in as a supplier")]
        public void GivenIHaveLoggedInAsASupplier()
        {
            generalHelpers.LoginAsSupplier();
        }

        [Given(@"navigate to Fuel Information page")]
        public void GivenNavigateToFuelInformationPage()
        {
            generalHelpers.NavigateToFuelInformationPage();
        }

        #endregion Stepdefinitions
    }
}