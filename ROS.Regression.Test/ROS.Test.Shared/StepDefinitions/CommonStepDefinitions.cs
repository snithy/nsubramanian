﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ROS.Test.Shared.Helpers;
using ROS.Test.Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace ROS.Test.Shared.StepDefinitions
{
    [Binding]

    public class CommonStepDefinitions
    {
        /// <summary>
        /// This class contains commonly used step definitions in all specflow UI test projects
        /// </summary>
        /// 
        #region Properties  

        private readonly GeneralHelpers generalHelpers = new GeneralHelpers();

        private readonly ScenarioContext _scenarioContext;

        #endregion Properties

        #region Constructor

        public CommonStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        #endregion Constructor


        #region Stepdefinitions

        [When(@"I sign out of the account")]
        [Then(@"I sign out of my account")]
        [Then(@"I sign out of the user account")]
        [Then(@"I sign out of the account")]
        public void ThenISignOutOfMyAccount()
        {
            generalHelpers.ClickElement("Sign out link");           

            Assert.IsTrue(
                Driver.Instance.PageSource.Contains("Successfully signed out")
                , "Failed to sign out of the account"
                );
        }

        #endregion Stepdefinitions
    }
}
