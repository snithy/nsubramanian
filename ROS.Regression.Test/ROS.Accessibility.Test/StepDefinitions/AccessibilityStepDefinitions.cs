﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ROS.Test.Shared.Utilities;
using Selenium.Axe;
using TechTalk.SpecFlow;

namespace ROS.Accessibility.Test.StepDefinitions
{
        [Binding]
        public class AccessibilityStepDefinitions
        {
            #region Properties

            private AxeResult AxeResult { get; set; }

            #endregion Properties

            #region Stepdefinitions

            /// <summary>
            /// Run aXe analysis against the page for accessibility violations
            /// </summary>        
            [When(@"I validate the page for accessibility violations")]
            public void WhenIValidateThePageForAccessibilityViolations()
            {
                var builder = new AxeBuilder(Driver.Instance);
                AxeResult = builder.Analyze();
            }

            /// <summary>
            /// Verify the number of accessibility violations in the page
            /// </summary>
            /// <param name="expNumberOfViolations"></param>        
            [Then(@"I should see (.*) violations")]
            public void ThenIShouldSeeViolations(int expNumberOfViolations)
            {
                int violationCount = AxeResult.Violations.Length;
                if (GetViolationCountExcludingAriaCurrent() == 0)
                {
                    violationCount = 0;
                }

                Assert.IsTrue(violationCount == expNumberOfViolations, ExtractResult(AxeResult.Violations).ToString());
            }

            #endregion Stepdefinitions

            #region Private Methods

            /// <summary>
            /// Extracts the result items from the results object
            /// </summary>
            /// <param name="violations"></param>
            /// <returns>Details of all accessibility violations on the page</returns>        
            private StringBuilder ExtractResult(AxeResultItem[] violations)
            {
                StringBuilder allViolations = new StringBuilder();

                allViolations.Append("\n\n" + "Found " + violations.Length + " accessibility violations:");
                allViolations.Append("\n Test page url: " + AxeResult.Url + "\n");

                int srlNumber = 1;
                foreach (var item in violations)
                {
                    allViolations.Append("\n" + srlNumber + ") ");
                    allViolations.Append("Violation type: " + item.Help.ToString() + ": " + item.HelpUrl.ToString() + "\n");
                    allViolations.Append("      " + item.Description.ToString() + "\n");
                    allViolations.Append("      Violation impact: " + item.Impact.ToString() + "\n");

                    foreach (var tagItem in item.Tags)
                    {
                        allViolations.Append("      Tags: " + tagItem.ToString() + "\n");
                    }

                    int nodeNumber = 1;

                    foreach (var nodeItem in item.Nodes)
                    {
                        allViolations.Append("      " + nodeNumber + ") ");
                        allViolations.Append("      Impact: " + nodeItem.Impact.ToString() + "\n");
                        allViolations.Append("      Html: " + nodeItem.Html.ToString() + "\n");

                        foreach (var tarItem in nodeItem.Target)
                        {
                            allViolations.Append("      Target: " + tarItem.ToString() + "\n");
                        }

                        allViolations.Append("      Fix the following issues: \n");

                        foreach (var anyItem in nodeItem.Any)
                        {
                            var msg = anyItem.Message;
                            allViolations.Append("          " + msg + "\n");
                        }
                        allViolations.Append("\n");
                        nodeNumber++;
                    }

                    srlNumber++;
                }

                return allViolations;
            }

            /// <summary>
            /// Counts the number of violations excluding Aria-Current
            /// </summary>
            /// <returns>Violation Count</returns>
            private int GetViolationCountExcludingAriaCurrent()
            {
                int violationCount = 0;

                foreach (var item in AxeResult.Violations)
                {
                    foreach (var nodeItem in item.Nodes)
                    {
                        foreach (var anyItem in nodeItem.Any)
                        {
                            var errMsg = anyItem.Message;
                            if (!(errMsg.Contains("aria-current") ||
                                errMsg.Contains("aria-expanded") ||
                                errMsg.Contains("aria-describedby")
                                ))
                            {
                                violationCount++;
                            }
                        }
                    }
                }
                return violationCount;
            }

            #endregion Private Methods
        }
    }
